---
layout: post
title:  "Tu Embarca"
categories: [android]
color: "#607d8b"
actioncolor: "#b0bec5"
techtags: [web services, json, maps]
androidtags: [material design, google maps, retrofit]
iostags: [maps,test]
cardImage: "android/tuemb/tuemb_1.jpg"
images: ["android/tuemb/tuemb_2.jpg","android/tuemb/tuemb_3.jpg","android/tuemb/tuemb_4.jpg","android/tuemb/tuemb_5.jpg"]
devices: ["Galaxy Note 3", "Nexus 7", "Moto G 3"]
---
Is an app for getting and setting the information related to the deliveries from
the drivers and clients from Tu Embarca. Unlike other apps that I have worked on,
I was the one to decide the look and feel of the app, therefore I completely built this app.
The principal features of the app are the following:
* For the drivers
  - Update location of packages using the **gps** of the device
  - Send offers from requests of the clients
  - Look to the nearest offers from the clients using **gps** and **maps**
* For the clients
  - Preview the location of the packages using the **map** features
  - Update and view the account data
