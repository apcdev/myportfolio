---
layout: post
title:  "CNSF citas"
categories: [android]
color: "#673ab7"
actioncolor: "#b39ddb"
techtags: [web services, json, maps, testflight]
cardImage: "android/cnsf/cnsf_1.png"
images: ["android/cnsf/cnsf_2.png","android/cnsf/cnsf_3.png","android/cnsf/cnsf_4.png"]
devices: [Galaxy Tab, Moto G, Galaxy Note 3]
---
Was an app for Mexico's insurance and surety bond regulator the Comision Nacional de Seguros y Fianzas (CNSF) and was
used for scheduling appointments for the insurance brokers.
Using a webservice made by me using **python** the app was able to achieve following:
* Allow the user to prefill the required data for scheduling appointment.
* Preview on **map** the locations of the different offices to schedule the appointments.
* Preview on a **custom calendar** the date availability for scheduling appointments
* Make appointments and visualize scheduled appointments
* Cypher through webservice interactions
