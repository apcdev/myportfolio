---
layout: default
title: Resources
type: aboutPage
---
<h5 class="about-title">Web resources</h5>

<div class="about-card mdl-shadow--4dp">

  <ul class="demo-list-two mdl-list">
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <span>Material design lite</span>
        <span class="mdl-list__item-text-body">Used for the frontend of this site
        <br />
        <a href="https://getmdl.io/" target="_blank" class="resource-link">Visit page</a>
        </span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <span>Jekyll</span>
        <span class="mdl-list__item-text-body">Used for building the core features of the site
        <br />
        <a href="http://jekyllrb.com/" target="_blank" class="resource-link">Visit page</a>
        </span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <span>Responsinator</span>
        <span class="mdl-list__item-text-body">Used for visualizing the site through many resolutions
        <br />
        <a href="http://www.responsinator.com/" target="_blank" class="resource-link">Visit page</a>
        </span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <span>Material mixer</span>
        <span class="mdl-list__item-text-body">Used for defining the colors for this site
        <br />
        <a href="http://materialmixer.co/" target="_blank" class="resource-link">Visit page</a>
        </span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <span>Magnific popup</span>
        <span class="mdl-list__item-text-body">Library used for amplifying the images in the site
        <br />
        <a href="http://dimsemenov.com/plugins/magnific-popup/" target="_blank" class="resource-link">Visit page</a>
        </span>
      </span>
    </li>
    <li class="mdl-list__item mdl-list__item--three-line">
      <span class="mdl-list__item-primary-content">
        <span>Formspree</span>
        <span class="mdl-list__item-text-body">Service for sending emails in static sites
        <br />
        <a href="https://formspree.io/" target="_blank" class="resource-link">Visit page</a>
        </span>
      </span>
    </li>
  </ul>

</div>
